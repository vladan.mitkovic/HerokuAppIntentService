package me.mitkovic.android.herokuappintentservice;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.LruCache;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTIST_KEY;

public class ArtistDetailActivity extends AppCompatActivity {

    private Button addToFavorite;
    private boolean inFavorites;

    private SharedPreferencesManager mSharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);

        Intent intent = getIntent();

        final Artist artist = intent.getParcelableExtra(ARTIST_KEY);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(artist.getArtist());
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);

        LruCache<String, Bitmap> mImageCache = HerokuApplication.getInstance().getImageCache();

        ImageView mImageView = (ImageView) findViewById(R.id.artist_image_view);
        mImageView.setImageBitmap(mImageCache.get(artist.getArtistId()));

        addToFavorite = (Button) findViewById(R.id.add_to_favorites);
        addToFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToSharedPreferences(artist);
            }
        });

        inFavorites = false;
        String favoriteProfile = mSharedPreferencesManager.getString("artist_" + artist.getArtistId());
        if (!favoriteProfile.equals("")) {
            inFavorites = true;
            addToFavorite.setText(getResources().getString(R.string.remove_from_favorites));
        }
    }

    private void saveToSharedPreferences(Artist artist) {
        if (artist == null) return;

        if (!inFavorites) {
            inFavorites = true;
            mSharedPreferencesManager.storeArtist("artist_" + artist.getArtistId(), artist);
            addToFavorite.setText(getResources().getString(R.string.remove_from_favorites));
        } else {
            inFavorites = false;
            mSharedPreferencesManager.deleteArtist("artist_" + artist.getArtistId());
            addToFavorite.setText(getResources().getString(R.string.add_to_favorites));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}