package me.mitkovic.android.herokuappintentservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.adapter.ArtistsAdapter;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.remote.DownloadBroadcastService;
import me.mitkovic.android.herokuappintentservice.utils.Constants;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTIST_KEY;

public class BroadcastReceiverActivity extends AppCompatActivity {

    private static final String TAG = BroadcastReceiverActivity.class.getSimpleName();

    private String profileURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private SharedPreferencesManager mSharedPreferencesManager;

    private ProgressBar mProgressBar;
    private GridView mArtistsGridView;
    private List<Artist> mArtists;
    private ArtistsAdapter mAdapter;

    private Toolbar mToolbar;

    DownloadStateReceiver mDownloadStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_receiver);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.broadcast_receiver));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mArtistsGridView = (GridView) findViewById(R.id.artists_grid_view);

        Gson gson = new Gson();
        mArtists = gson.fromJson(mSharedPreferencesManager.getString(ARTISTS_KEY), new TypeToken<List<Artist>>(){}.getType());

        mAdapter = new ArtistsAdapter(BroadcastReceiverActivity.this, mArtists, mSharedPreferencesManager);
        mArtistsGridView.setAdapter(mAdapter);

        mArtistsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = mArtists.get(position);

                Intent intent = new Intent(BroadcastReceiverActivity.this, ArtistDetailActivity.class);
                intent.putExtra(ARTIST_KEY, artist);

                startActivity(intent);
            }
        });

        IntentFilter statusIntentFilter = new IntentFilter(Constants.BROADCAST_ACTION);
        statusIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        mDownloadStateReceiver = new DownloadStateReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloadStateReceiver, statusIntentFilter);

        fetchData();
    }

    private void fetchData() {
        mProgressBar.setVisibility(View.VISIBLE);

        Intent intent = new Intent(this, DownloadBroadcastService.class);
        intent.setData(Uri.parse(profileURL));
        startService(intent);
    }

    private class DownloadStateReceiver extends BroadcastReceiver {

        private DownloadStateReceiver() {
            // prevents instantiation by other packages.
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getIntExtra(Constants.EXTENDED_DATA_STATUS, Constants.STATE_ACTION_COMPLETE)) {
                case Constants.STATE_ACTION_STARTED:

                    mProgressBar.setVisibility(View.VISIBLE);

                    Log.d(TAG, getResources().getString(R.string.service_started));
                    Utils.showToast(context, getResources().getString(R.string.service_started));

                    break;
                case Constants.STATE_ACTION_CONNECTING:

                    Log.d(TAG, getResources().getString(R.string.service_connecting));
                    Utils.showToast(context, getResources().getString(R.string.service_connecting));

                    break;
                case Constants.STATE_ACTION_PARSING:

                    Log.d(TAG, getResources().getString(R.string.service_result_data_parsing));
                    Utils.showToast(context, getResources().getString(R.string.service_result_data_parsing));

                    break;
                case Constants.STATE_ACTION_WRITING:

                    Log.d(TAG, getResources().getString(R.string.service_result_data_writing));
                    Utils.showToast(context, getResources().getString(R.string.service_result_data_writing));

                    break;
                case Constants.STATE_ACTION_PROBLEM:

                    mProgressBar.setVisibility(View.GONE);

                    Log.d(TAG, getResources().getString(R.string.service_problem));
                    Utils.showToast(context, getResources().getString(R.string.service_problem));

                    break;
                case Constants.STATE_ACTION_COMPLETE:

                    mProgressBar.setVisibility(View.GONE);

                    Log.d(TAG, getResources().getString(R.string.service_completed));
                    Utils.showToast(context, getResources().getString(R.string.service_completed));

                    mArtists = intent.getParcelableArrayListExtra(Constants.EXTENDED_DATA);
                    mAdapter.updateResults(mArtists);

                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDownloadStateReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mDownloadStateReceiver);
            mDownloadStateReceiver = null;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}