package me.mitkovic.android.herokuappintentservice;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.facebook.stetho.Stetho;

import me.mitkovic.android.herokuappintentservice.remote.ConnectivityReceiver;

public class HerokuApplication extends Application {

    private static HerokuApplication mInstance;
    private static LruCache<String, Bitmap> mImageCache;

    public static synchronized HerokuApplication getInstance() {
        return mInstance;
    }

    public void onCreate() {
        super.onCreate();

        mInstance = this;

        Stetho.initializeWithDefaults(this);
    }

    public void setConnectivityListener(ConnectivityReceiver.IConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public LruCache<String, Bitmap> getImageCache() {
        if(mImageCache == null) {
            int maxMemory = (int)(Runtime.getRuntime().maxMemory() / 1024);
            int cacheSize = maxMemory / 8;
            mImageCache = new LruCache<>(cacheSize);
        }
        return mImageCache;
    }

}