package me.mitkovic.android.herokuappintentservice;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import me.mitkovic.android.herokuappintentservice.remote.ConnectivityReceiver;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.IConnectivityReceiverListener {

    private SharedPreferencesManager mSharedPreferencesManager;

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(mToolbar);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);

        Button goToSharedPreferences = (Button) findViewById(R.id.goToSharedPreferences);
        goToSharedPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, SharedPreferencesActivity.class);
                startActivity(intent);
            }
        });

        Button goToBroadcastReceiver = (Button) findViewById(R.id.goToBroadcastReceiver);
        goToBroadcastReceiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, BroadcastReceiverActivity.class);
                startActivity(intent);
            }
        });

        Button goToOttoBus = (Button) findViewById(R.id.goToOttoBus);
        goToOttoBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, OttoBusActivity.class);
                startActivity(intent);
            }
        });

        Button gotTomMessengerHandler = (Button) findViewById(R.id.gotTomMessengerHandler);
        gotTomMessengerHandler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, MessengerHandlerActivity.class);
                startActivity(intent);
            }
        });

        Button gotToThreadPool = (Button) findViewById(R.id.gotToThreadPool);
        gotToThreadPool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ThreadPoolActivity.class);
                startActivity(intent);
            }
        });

        Button clearSharedPreferences = (Button) findViewById(R.id.clearSharedPreferences);
        clearSharedPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSharedPreferencesManager.deleteAllKeys();

                Utils.showToast(getApplicationContext(), getResources().getString(R.string.shared_preferences_cleared));
            }
        });

        checkConnection();
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = getResources().getString(R.string.connected_to_internet);
            color = Color.WHITE;
        } else {
            message = getResources().getString(R.string.not_connected_to_internet);
            color = Color.RED;
        }

        Snackbar snackbar = Snackbar.make(findViewById(R.id.main_activity_layout), message, Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        HerokuApplication.getInstance().setConnectivityListener(this);
    }

}