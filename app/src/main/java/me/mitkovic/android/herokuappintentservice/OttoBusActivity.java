package me.mitkovic.android.herokuappintentservice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.otto.Subscribe;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.adapter.ArtistsAdapter;
import me.mitkovic.android.herokuappintentservice.bus.GlobalBus;
import me.mitkovic.android.herokuappintentservice.busevents.OnDataDownloadProblem;
import me.mitkovic.android.herokuappintentservice.busevents.OnDataDownloaded;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.remote.DownloadOttoBusService;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTIST_KEY;

public class OttoBusActivity extends AppCompatActivity {

    private static final String TAG = OttoBusActivity.class.getSimpleName();

    private String profileURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private SharedPreferencesManager mSharedPreferencesManager;

    private ProgressBar mProgressBar;
    private GridView mArtistsGridView;
    private List<Artist> mArtists;
    private ArtistsAdapter mAdapter;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otto_bus);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.otto_bus));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mArtistsGridView = (GridView) findViewById(R.id.artists_grid_view);

        Gson gson = new Gson();
        mArtists = gson.fromJson(mSharedPreferencesManager.getString(ARTISTS_KEY), new TypeToken<List<Artist>>(){}.getType());

        mAdapter = new ArtistsAdapter(OttoBusActivity.this, mArtists, mSharedPreferencesManager);
        mArtistsGridView.setAdapter(mAdapter);

        mArtistsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = mArtists.get(position);

                Intent intent = new Intent(OttoBusActivity.this, ArtistDetailActivity.class);
                intent.putExtra(ARTIST_KEY, artist);

                startActivity(intent);
            }
        });

        fetchData();
    }

    private void fetchData() {
        mProgressBar.setVisibility(View.VISIBLE);

        Intent intent = new Intent(this, DownloadOttoBusService.class);
        intent.setData(Uri.parse(profileURL));
        startService(intent);
    }

    @Override
    public void onStart() {
        super.onStart();

        GlobalBus.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        GlobalBus.getInstance().unregister(this);
    }

    @Subscribe
    public void onDataDownloaded(OnDataDownloaded downloadedData) {

        mProgressBar.setVisibility(View.GONE);

        Utils.showToast(getApplicationContext(), getResources().getString(R.string.service_completed));

        mArtists = downloadedData.getArtists();
        mAdapter.updateResults(mArtists);
    }

    @Subscribe
    public void onDataDownloadProblem(OnDataDownloadProblem downloadedData) {

        mProgressBar.setVisibility(View.GONE);

        Utils.showToast(getApplicationContext(), downloadedData.getProblemDescription());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}