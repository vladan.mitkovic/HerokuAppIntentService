package me.mitkovic.android.herokuappintentservice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.adapter.ArtistsAdapter;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.remote.DownloadIntentService;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTIST_KEY;

public class SharedPreferencesActivity extends AppCompatActivity implements OnSharedPreferenceChangeListener {

    private String profileURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private SharedPreferencesManager mSharedPreferencesManager;

    private GridView mArtistsGridView;
    private List<Artist> mArtists;
    private ArtistsAdapter mAdapter;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.shared_preferences));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
        mSharedPreferencesManager.registerListener(this);

        mArtistsGridView = (GridView) findViewById(R.id.artists_grid_view);

        Gson gson = new Gson();
        mArtists = gson.fromJson(mSharedPreferencesManager.getString(ARTISTS_KEY), new TypeToken<List<Artist>>(){}.getType());

        mAdapter = new ArtistsAdapter(SharedPreferencesActivity.this, mArtists, mSharedPreferencesManager);
        mArtistsGridView.setAdapter(mAdapter);

        mArtistsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = mArtists.get(position);

                Intent intent = new Intent(SharedPreferencesActivity.this, ArtistDetailActivity.class);
                intent.putExtra(ARTIST_KEY, artist);

                startActivity(intent);
            }
        });

        fetchData();
    }

    private void fetchData() {
        Intent intent = new Intent(this, DownloadIntentService.class);
        intent.setData(Uri.parse(profileURL));
        startService(intent);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Utils.showToast(getApplicationContext(), getResources().getString(R.string.service_completed));

        Gson gson = new Gson();
        mArtists = gson.fromJson(mSharedPreferencesManager.getString(ARTISTS_KEY), new TypeToken<List<Artist>>(){}.getType());

        mAdapter.updateResults(mArtists);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        mSharedPreferencesManager.unRegisterListener(this);
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}