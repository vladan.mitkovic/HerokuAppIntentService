package me.mitkovic.android.herokuappintentservice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.WeakReference;
import java.util.List;

import me.mitkovic.android.herokuappintentservice.adapter.ArtistsAdapter;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.remote.ThreadPoolDownloadService;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTIST_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.MESSENGER_KEY;

public class ThreadPoolActivity extends AppCompatActivity {

    private String profileURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private SharedPreferencesManager mSharedPreferencesManager;

    private GridView mArtistsGridView;
    private List<Artist> mArtists;
    private ArtistsAdapter mAdapter;

    private Toolbar mToolbar;

    private MessengerHandler handler = new MessengerHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_pool);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.thread_pool_download_service));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);

        mArtistsGridView = (GridView) findViewById(R.id.artists_grid_view);

        Gson gson = new Gson();
        mArtists = gson.fromJson(mSharedPreferencesManager.getString(ARTISTS_KEY), new TypeToken<List<Artist>>(){}.getType());

        mAdapter = new ArtistsAdapter(ThreadPoolActivity.this, mArtists, mSharedPreferencesManager);
        mArtistsGridView.setAdapter(mAdapter);

        mArtistsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = mArtists.get(position);

                Intent intent = new Intent(ThreadPoolActivity.this, ArtistDetailActivity.class);
                intent.putExtra(ARTIST_KEY, artist);

                startActivity(intent);
            }
        });

        fetchData();
    }

    private void fetchData() {
        Messenger messenger = new Messenger(handler);

        Intent intent = new Intent(ThreadPoolActivity.this, ThreadPoolDownloadService.class);
        intent.putExtra(MESSENGER_KEY, messenger);
        intent.setData(Uri.parse(profileURL));

        startService(intent);
    }

    private static class MessengerHandler extends Handler {

        WeakReference<ThreadPoolActivity> mActivity;

        public MessengerHandler(ThreadPoolActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final ThreadPoolActivity activity = mActivity.get();
            if (activity != null) {
                Bundle data = msg.getData();
                activity.displayArtists(data);
            }
        }
    }

    private void displayArtists(Bundle data) {
        Utils.showToast(getApplicationContext(), getResources().getString(R.string.service_completed));

        mArtists = data.getParcelableArrayList(ARTISTS_KEY);
        mAdapter.updateResults(mArtists);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}