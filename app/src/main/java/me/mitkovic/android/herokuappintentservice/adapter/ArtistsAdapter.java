package me.mitkovic.android.herokuappintentservice.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.List;

import me.mitkovic.android.herokuappintentservice.HerokuApplication;
import me.mitkovic.android.herokuappintentservice.R;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;

public class ArtistsAdapter extends BaseAdapter {

    private Context mContext;
    private List<Artist> mArtistsList;
    private static LruCache<String, Bitmap> mImageCache;
    private SharedPreferencesManager mSharedPreferencesManager;
    private  LayoutInflater inflater;

    public ArtistsAdapter(Context context, List<Artist> artistsList, SharedPreferencesManager sharedPreferencesManager) {
        mContext = context;
        mArtistsList = artistsList;
        mImageCache = HerokuApplication.getInstance().getImageCache();
        mSharedPreferencesManager = sharedPreferencesManager;

        inflater = ((Activity) mContext).getLayoutInflater();
    }

    @Override
    public int getCount() {
        if (mArtistsList == null) return 0;
        return mArtistsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArtistsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateResults(List<Artist> results) {
        mArtistsList = results;

        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.thumbnail_view, null);

            holder = new ViewHolder();

            holder.imageView = (ImageView) convertView.findViewById(R.id.image_thumbnail);
            holder.starImageView = (ImageView) convertView.findViewById(R.id.image_favorite);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Artist artist = mArtistsList.get(position);
        if (artist != null) {
            if (holder.imageView != null) {
                Bitmap bitmap = mImageCache.get(artist.getArtistId());
                if (bitmap == null) {
                    new ImageDownloaderTask(holder.imageView).execute(artist.getImage(), artist.getArtistId());
                } else {
                    holder.imageView.setImageBitmap(bitmap);
                    holder.imageView.setAdjustViewBounds(true);
                }
            }

            String favoriteProfile = mSharedPreferencesManager.getString("artist_" + artist.getArtistId());
            if (!favoriteProfile.equals("")) {
                holder.starImageView.setVisibility(View.VISIBLE);
            } else {
                holder.starImageView.setVisibility(View.GONE);
            }
        }
        return convertView;
    }

    private static class ViewHolder {
        ImageView starImageView;
        ImageView imageView;
    }

    private static class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {

        private final WeakReference<ImageView> imageViewReference;

        public ImageDownloaderTask(ImageView imageView) {
            imageViewReference = new WeakReference<>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            if (!isCancelled()) {
                bitmap = downloadImageFile(params[0]);
                if (bitmap != null) {
                    mImageCache.put(params[1], bitmap);
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                        imageView.setAdjustViewBounds(true);
                    }
                }
            }
        }

        private Bitmap downloadImageFile(String url) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

}