package me.mitkovic.android.herokuappintentservice.bus;

import android.os.Handler;
import android.os.Looper;

public class GlobalBus extends com.squareup.otto.Bus {

    private static GlobalBus mInstance;

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public static GlobalBus getInstance() {
        if (mInstance == null) {
            synchronized (GlobalBus.class) {
                if (mInstance == null) {
                    mInstance = new GlobalBus();
                }
            }
        }
        return mInstance;
    }

    @Override
    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    GlobalBus.super.post(event);
                }
            });
        }
    }
}