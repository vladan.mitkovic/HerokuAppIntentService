package me.mitkovic.android.herokuappintentservice.busevents;

public class OnDataDownloadProblem {

    private String mProblemText;

    public OnDataDownloadProblem(String problemText) {
        this.mProblemText = problemText;
    }

    public String getProblemDescription() {
        return mProblemText;
    }

}