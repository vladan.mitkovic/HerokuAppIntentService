package me.mitkovic.android.herokuappintentservice.busevents;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;

public class OnDataDownloaded {

    private List<Artist> mArtists;

    public OnDataDownloaded(List<Artist> artists) {
        this.mArtists = artists;
    }

    public List<Artist> getArtists() {
        return mArtists;
    }

}