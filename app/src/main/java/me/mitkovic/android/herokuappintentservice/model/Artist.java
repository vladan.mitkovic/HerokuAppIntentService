package me.mitkovic.android.herokuappintentservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Artist implements Parcelable {

    String title;
    String artist;
    String artist_id;
    String image;

    public Artist() {}

    public Artist(Parcel in) {
        title = in.readString();
        artist = in.readString();
        artist_id = in.readString();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(artist_id);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtistId() {
        return artist_id;
    }

    public void setArtistId(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @SuppressWarnings("unused")
    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    @Override
    public String toString() {
        return "{" + "artist_id= " + artist_id + " -- " + "artist= " + artist + " -- " + "title= " + title + " -- " + "image= " + image + " }";
    }
}
