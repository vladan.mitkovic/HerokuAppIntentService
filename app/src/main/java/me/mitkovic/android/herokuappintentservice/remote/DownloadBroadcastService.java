package me.mitkovic.android.herokuappintentservice.remote;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.BroadcastNotifier;
import me.mitkovic.android.herokuappintentservice.utils.Constants;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;

public class DownloadBroadcastService extends IntentService {

    private static final String TAG = DownloadBroadcastService.class.getName();

    private BroadcastNotifier mBroadcaster = new BroadcastNotifier(this);

    private SharedPreferencesManager mSharedPreferencesManager;
    private List<Artist> mArtists;

    public DownloadBroadcastService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getData();

        HttpHandler httpHandler = new HttpHandler();
        String jsonString = httpHandler.loadFromNetwork(uri.toString(), mBroadcaster);

        mArtists = Utils.getArtistsList(jsonString, mBroadcaster);

        if (mArtists != null) {
            mBroadcaster.broadcastIntentWithState(Constants.STATE_ACTION_WRITING);

            mSharedPreferencesManager.storeArtists(ARTISTS_KEY, mArtists);

            mBroadcaster.broadcastIntentWithData(Constants.STATE_ACTION_COMPLETE, new ArrayList<>(mArtists));
        } else {

            mBroadcaster.broadcastIntentWithData(Constants.STATE_ACTION_PROBLEM, null);
        }
    }

}