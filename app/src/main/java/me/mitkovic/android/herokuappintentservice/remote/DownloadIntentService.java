package me.mitkovic.android.herokuappintentservice.remote;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;

public class DownloadIntentService extends IntentService {

    private static final String TAG = DownloadIntentService.class.getName();

    private SharedPreferencesManager mSharedPreferencesManager;
    private List<Artist> mArtists;

    public DownloadIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getData();

        HttpHandler httpHandler = new HttpHandler();
        String jsonString = httpHandler.loadFromNetwork(uri.toString(), null);

        mArtists = Utils.getArtistsList(jsonString, null);

        if (mArtists != null) {
            mSharedPreferencesManager.storeArtists(ARTISTS_KEY, mArtists);
        }
    }

}