package me.mitkovic.android.herokuappintentservice.remote;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.MESSENGER_KEY;

public class DownloadMessengerHandlerService extends IntentService {

    private static final String TAG = DownloadMessengerHandlerService.class.getName();

    private SharedPreferencesManager mSharedPreferencesManager;
    private List<Artist> mArtists;

    public DownloadMessengerHandlerService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Uri uri = intent.getData();

        Messenger messenger = (Messenger) intent.getExtras().get(MESSENGER_KEY);

        HttpHandler httpHandler = new HttpHandler();
        String jsonString = httpHandler.loadFromNetwork(uri.toString(), null);

        mArtists = Utils.getArtistsList(jsonString, null);

        if (mArtists != null) {

            Message msg = Message.obtain();

            Bundle data = new Bundle();
            data.putParcelableArrayList(ARTISTS_KEY, new ArrayList<>(mArtists));

            msg.setData(data);
            try {
                //Send the Message back to the caller Activity.
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            mSharedPreferencesManager.storeArtists(ARTISTS_KEY, mArtists);
        }
    }

}