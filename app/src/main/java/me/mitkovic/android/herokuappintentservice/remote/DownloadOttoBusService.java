package me.mitkovic.android.herokuappintentservice.remote;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.R;
import me.mitkovic.android.herokuappintentservice.bus.GlobalBus;
import me.mitkovic.android.herokuappintentservice.busevents.OnDataDownloadProblem;
import me.mitkovic.android.herokuappintentservice.busevents.OnDataDownloaded;
import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;

public class DownloadOttoBusService extends IntentService {

    private static final String TAG = DownloadOttoBusService.class.getName();

    private SharedPreferencesManager mSharedPreferencesManager;
    private List<Artist> mArtists;

    public DownloadOttoBusService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getData();

        HttpHandler httpHandler = new HttpHandler();
        String jsonString = httpHandler.loadFromNetwork(uri.toString(), null);

        mArtists = Utils.getArtistsList(jsonString, null);

        if (mArtists != null) {
            mSharedPreferencesManager.storeArtists(ARTISTS_KEY, mArtists);

            GlobalBus.getInstance().post(new OnDataDownloaded(mArtists));
        } else {
            GlobalBus.getInstance().post(new OnDataDownloadProblem(getResources().getString(R.string.service_problem)));
        }
    }

}