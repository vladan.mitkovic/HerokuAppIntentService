package me.mitkovic.android.herokuappintentservice.remote;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import me.mitkovic.android.herokuappintentservice.utils.BroadcastNotifier;
import me.mitkovic.android.herokuappintentservice.utils.Constants;

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();

    public HttpHandler() {}

    public String loadFromNetwork(String requestUrl, BroadcastNotifier broadcastNotifier) {
        InputStream inputStream = null;
        String string = "";

        try {
            inputStream = downloadUrl(requestUrl, broadcastNotifier);
            string = convertStreamToString(inputStream);
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return string;
    }

    private InputStream downloadUrl(String requestUrl, BroadcastNotifier broadcastNotifier) throws IOException {
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(requestUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            if (broadcastNotifier != null) broadcastNotifier.broadcastIntentWithState(Constants.STATE_ACTION_STARTED);

            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);

            urlConnection.connect();

            if (broadcastNotifier != null) broadcastNotifier.broadcastIntentWithState(Constants.STATE_ACTION_CONNECTING);

            inputStream = urlConnection.getInputStream();

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return inputStream;

    }

    private String convertStreamToString(InputStream inputStream) {
        if (inputStream == null) return null;

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}