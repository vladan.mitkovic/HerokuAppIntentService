package me.mitkovic.android.herokuappintentservice.remote;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.mitkovic.android.herokuappintentservice.model.Artist;
import me.mitkovic.android.herokuappintentservice.utils.SharedPreferencesManager;
import me.mitkovic.android.herokuappintentservice.utils.Utils;

import static me.mitkovic.android.herokuappintentservice.utils.Constants.ARTISTS_KEY;
import static me.mitkovic.android.herokuappintentservice.utils.Constants.MESSENGER_KEY;

public class ThreadPoolDownloadService extends Service {

    static final int MAX_THREADS = 4;

    private ExecutorService mExecutor;

    private SharedPreferencesManager mSharedPreferencesManager;
    private List<Artist> mArtists;

    @Override
    public void onCreate() {
        mExecutor = Executors.newFixedThreadPool(MAX_THREADS);

        mSharedPreferencesManager = SharedPreferencesManager.getPreferences(this);
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        Runnable downloadRunnable = new Runnable() {

            @Override
            public void run() {

                Uri uri = intent.getData();

                Messenger messenger = (Messenger) intent.getExtras().get(MESSENGER_KEY);

                HttpHandler httpHandler = new HttpHandler();
                String jsonString = httpHandler.loadFromNetwork(uri.toString(), null);

                mArtists = Utils.getArtistsList(jsonString, null);

                if (mArtists != null) {

                    Message msg = Message.obtain();

                    Bundle data = new Bundle();
                    data.putParcelableArrayList(ARTISTS_KEY, new ArrayList<>(mArtists));

                    msg.setData(data);
                    try {
                        //Send the Message back to the caller Activity.
                        messenger.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    mSharedPreferencesManager.storeArtists(ARTISTS_KEY, mArtists);
                }
            }
        };
        mExecutor.execute(downloadRunnable);

        // Tell the Android framework how to behave if this service is
        // interrupted. In our case, we want to restart the service
        // then re-deliver the intent so that all files are eventually
        // downloaded.
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        mExecutor.shutdown();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}