package me.mitkovic.android.herokuappintentservice.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

import me.mitkovic.android.herokuappintentservice.model.Artist;

public class BroadcastNotifier {

    private LocalBroadcastManager mBroadcaster;

    public BroadcastNotifier(Context context) {
        mBroadcaster = LocalBroadcastManager.getInstance(context);
    }

    public void broadcastIntentWithData(int status, ArrayList<Artist> artistList) {

        Intent localIntent = new Intent();

        localIntent.setAction(Constants.BROADCAST_ACTION);

        localIntent.putExtra(Constants.EXTENDED_DATA_STATUS, status);
        localIntent.putParcelableArrayListExtra(Constants.EXTENDED_DATA, artistList);
        localIntent.addCategory(Intent.CATEGORY_DEFAULT);

        mBroadcaster.sendBroadcast(localIntent);
    }

    public void broadcastIntentWithState(int status) {

        Intent localIntent = new Intent();

        localIntent.setAction(Constants.BROADCAST_ACTION);

        localIntent.putExtra(Constants.EXTENDED_DATA_STATUS, status);
        localIntent.addCategory(Intent.CATEGORY_DEFAULT);

        mBroadcaster.sendBroadcast(localIntent);
    }
}