package me.mitkovic.android.herokuappintentservice.utils;

public final class Constants {

    public static final String BROADCAST_ACTION = "me.mitkovic.android.herokuappintentservice.BROADCAST";
    public static final String EXTENDED_DATA_STATUS = "me.mitkovic.android.herokuappintentservice.STATUS";
    public static final String EXTENDED_DATA = "me.mitkovic.android.herokuappintentservice.DATA";

    public static final String MESSENGER_KEY = "MESSENGER";
    public static final String ARTISTS_KEY = "ARTISTS";
    public static final String ARTIST_KEY = "ARTIST";

    // Status values to broadcast to the Activity

    // The download is starting
    public static final int STATE_ACTION_STARTED = 0;

    // The background thread is connecting to the internet
    public static final int STATE_ACTION_CONNECTING = 1;

    // The background thread is parsing the JSON
    public static final int STATE_ACTION_PARSING = 2;

    // The background thread is writing data to the shared prefernces
    public static final int STATE_ACTION_WRITING = 3;

    // The background thread is done
    public static final int STATE_ACTION_COMPLETE = 4;

    // The download problem
    public static final int STATE_ACTION_PROBLEM = 5;
}
