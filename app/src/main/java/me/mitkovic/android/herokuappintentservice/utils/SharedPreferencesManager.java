package me.mitkovic.android.herokuappintentservice.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import com.google.gson.Gson;

import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;

public class SharedPreferencesManager {

    private String EMPTY_STRING = "";

    private static SharedPreferences mPreferences;
    private static SharedPreferencesManager mInstance;

    private static String mPreferencesName = "HerokuApp";

    private static void initPreferences(final Context context) {
        mPreferences = context.getSharedPreferences(mPreferencesName, Activity.MODE_PRIVATE);
    }

    private SharedPreferencesManager(final Context context) {
        initPreferences(context);
    }

    public static SharedPreferencesManager getPreferences(final Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferencesManager(context);
        }
        return mInstance;
    }

    public void registerListener(OnSharedPreferenceChangeListener listener) {
        mPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unRegisterListener(OnSharedPreferenceChangeListener listener) {
        mPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public void storeString(final String key, final String value) {
        Editor editor = mPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(final String key) {
        return mPreferences.getString(key, EMPTY_STRING);
    }

    public <T> void storeArtist(String key, Artist artist) {
        Gson gson = new Gson();
        String json = gson.toJson(artist);

        set(key, json);
    }

    public <T> void storeArtists(String key, List<Artist> artists) {
        Gson gson = new Gson();
        String json = gson.toJson(artists);

        set(key, json);
    }

    public void deleteArtist(String key) {
        Editor editor = mPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public void deleteAllKeys() {
        Editor editor = mPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void set(String key, String value) {
        storeString(key, value);
    }

}