package me.mitkovic.android.herokuappintentservice.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.mitkovic.android.herokuappintentservice.model.Artist;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static List<Artist> getArtistsList(String jsonString, BroadcastNotifier broadcastNotifier) {
        if (jsonString == null) return null;

        List<Artist> artistsList = new ArrayList<>();

        if (broadcastNotifier != null) broadcastNotifier.broadcastIntentWithState(Constants.STATE_ACTION_PARSING);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONObject playlist = jsonObj.getJSONObject("playlist");
            JSONArray artists = playlist.getJSONArray("a");

            for (int i = 0; i < artists.length(); i++) {
                JSONObject artistsJSONObject = artists.getJSONObject(i);

                String artistId = artistsJSONObject.getString("artist_id");
                String title = artistsJSONObject.getString("title");
                String artistName = artistsJSONObject.getString("artist");
                String image = artistsJSONObject.getString("image");

                Artist artist = new Artist();
                artist.setArtistId(artistId);
                artist.setTitle(title);
                artist.setArtist(artistName);
                artist.setImage(image);

                artistsList.add(artist);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
        }

        return artistsList;
    }

}